#import "theorems.typ": *

#let definition = thmbox("definition", "Definition")

#text("A Categorical Semantics for SPARC TSO", size: 2em)

This note sketches a categorical semantics for the SPARC TSO weak memory model adapted from the denotational semantics given in @den-sparc. The goal is to eventually use this semantics as an example model of `isotope` to show the compatibility of our basic optimization techniques with weak memory models. In particular, our modifications have the following goals:
- To show that sequential composition is a monoid with $bold("skip")$ as the identity
- To show that the weak memory effects introduced form a strong monad over $sans("Set")$, and hence induce the structure of a premonoidal category
- To show that the weak memory monad is Elgot (as defined in @coind-res), and hence, via the inherited coproducts from $sans("Set")$, that its Kliesli category is an `isotope` model.

#outline()

#set heading(numbering: "1 ")

= Pomsets

We begin by fixing a countably infinite universe $cal(U)$, which we assume to contain all finite products and countable coproducts. We also fix some basic notation:
- Given a binary relation $R ⊆ A × A$, we define its *binary restriction* $R_B = R ∩ B × B ⊆ B × B$
- Given a function $φ: A -> B$ and $C ⊆ A$, we define its *restriction* $φ_C = φ ∩ C × B: C -> B$
#definition(name: "Pomset")[
    A (strict) *partially-ordered multiset* or *pomset* $(P, <_P, Φ)$ over a *label set* $L$ consists of a strict poset $(P, <_P)$ (with $P ⊆ cal(U)$) of *action occurences* and a function $Φ: P -> L$ mapping each occurence to its *label* or *action*. A pomset is *finite (countable)* if its underlying set $P$ is finite (countable).

    We say two pomsets $(P, <_P, Φ)$, $(Q, <_Q, Ψ)$ are *isomorphic* if there exists a poset isomorphism $φ: (P, <_P) ≅ (Q, <_Q)$ such that $Φ = Ψ ∘ φ$. Sequential composition and parallel composition of isomorphism classes of pomsets are defined pairwise; removal is defined with respect to a representative. We denote the function sending a pomset to its isomorphism class as $sans("I")$.
]

We define the following basic operations on pomsets $(P, <_P, Φ)$, $(Q, <_Q, Ψ)$:
- The *sequential composition* $(P, <_P, Φ);(Q, <_Q, Ψ)$, given by
   $(P ⊔ Q, <_(P;Q), Φ ⊔ Ψ)$, where $<_(P;Q)$ is the smallest partial order such that $(<_(P;Q))_P = <_P$, $(<_(P;Q))_Q = <_Q$, and $∀p ∈ P, q ∈ Q, p <_(P;Q) q$
- The *parallel composition* $(P, <_P, Φ)||(Q, <_Q, Ψ)$, given by $(P + Q, <_P + <_Q, Φ + Ψ)$.
- The *deletion* of $R$ from $(P, <_P, Φ)$, given by $(P slash R, <_P ∩ ((P slash R) × (P slash R)), Φ ∩ (P slash R))$.
- Given a poset $(N, <_N)$ and an $N$-indexed family of pomsets 
    $I = λ n.(P_n, <_n, Φ_n)$, we may define the *sequencing* of $I$
    $
    sans("seq")(<_N, I) = (∑_(n ∈ N)P_n, λ(i, p) (j, q). i < j ∨ (i = j ∧ p <_i q), ∑_(n ∈ N)Φ_n)
    $
    In particular, we have
    $
    (P_0, <_0, Φ_0);(P_1, <_1, Φ_1) &≅ sans("seq")({0, 1}, {0 < 1}, {(P_i, <_i, Φ_i)}) \
    (P_0, <_0, Φ_0)||(P_1, <_1, Φ_1) &≅ sans("seq")({0, 1}, {}, {(P_i, <_i, Φ_i)})
    $ 
    for all $P_0, P_1$ (taking $P_0 + P_1 = ∑_(i ∈ {0, 1})P_i$, which is a sensible definition; otherwise, this only holds up to isomorphism).

    Where the order $<_N$ is clear from context, we sometimes simply write $sans("seq")(N, I)$ or even just $sans("seq")(I)$.
- Given a poset $(N, <_N)$ and a pomset $p$, we define the *repetition* of $p$ by $N$ as
    $
    (N, <_N) * p = sans("seq")(N, <_N, λ n. p)
    $
    Where the order $<_N$ is clear from context, we may omit it.
We begin by stating the following basic identities:
- $(p_0;p_1);p_2 ≅ p_0;(p_1;p_2) ≅ sans("seq")({0, 1, 2}, <_ℕ, λ n.p_n)$
- $(p_0||p_1)||p_2 ≅ p_0||(p_1||p_2) ≅ sans("seq")({0, 1, 2}, {}, λ n.p_n)$
- Where $0 ∈ N$, $sans("seq")(N, <_ℕ, I) ≅ I_0;sans("seq")(N slash 0, <_ℕ, I)$; in particular, 
    - $ℕ * p ≅ p;(ℕ * p)$ 
    - $(n + 1) * p ≅ p;(n * p)$.
We prove that sequential composition, parallel composition, and sequencing distribute appropriately over isomorphism classes as follows:
- $sans("I")(sans("seq")(N, <_N, I)) ⊇ {sans("seq")(N, <_N, J) | ∀n, J_n ∈ sans("I")(I_n)}$: 
    let $(P_i', <_i', Φ_i') ∈ sans("I")((P_i, <_i, Φ_i))$. By definition, we have pomset isomorphisms $φ_i: (P_i, <_i, Φ_i) -> (P_i', <_i', Φ_i')$. Consider the function
    $
    φ = Σ_i φ_i: Σ_i P_i → Σ_i P_i'
    $
    and let 
    $
    (P, <, Φ) &= sans("seq")(N, <_N, λ n. (P_n, <_n, Φ_n)) \
    (P', <', Φ') &= sans("seq")(N, <_N, λ n. (P_n', <_n', Φ_n'))
    $
    We have that:
    $
    (Σ_i Φ_i') ∘ (Σ_i φ_i) &= Σ_i (Φ_i' ∘ φ_i) = Σ_i Φ_i = Φ \
    (i, p) < (j, q) &<==> (i <_N j) ∨ (i = j ∧ p <_(P_i) q) 
    \ &==> (i <_N j) ∨ (i = j ∧ φ_i(p) <_(P_i') φ_i(q))
    \ &<==> (i, φ_i(p)) <' (j, φ_j(q))
    \ &<==> φ((i, p)) <' φ((j, q))
    $
    Hence, since $φ$ is a bijection, it follows that it is a witness that $(P', <', Φ') ≅ (P, <, Φ)$ and therefore that
    $
    (P', <', Φ') ∈ sans("I")((P, <, Φ))
    $
    as desired.
- $sans("I")(p_0;p_1) ⊇ {q_0;q_1 | q_i ∈ sans("I")(p_i)}$: it suffices to instantiate the result for $sans("seq")$ with $N = {0, 1}$, $<_N = {0 < 1}$.
- $sans("I")(p_0||p_1) ⊇ {q_0||q_1 | q_i ∈ sans("I")(p_i)}$: it suffices to instantiate the result for $sans("seq")$ with $N = {0, 1}$, $<_N = {}$.
- $sans("I")((N, <_N) * p) ⊇ {N * q | q ∈ sans("I")(p)}$: it suffices to instantiate the result for $sans("seq")$ with $I = λ n. p$.

We may hence define the sequential composition, parallel composition, and sequencing of isomorphism classes of pomsets as follows:
$
sans("seq")(N, <_N, λ n.sans("I")(p_i)) &= sans("I")(sans("seq")(N, <_N, p_i)) \
sans("I")(p);sans("I")(q) &= sans("I")(p;q) \
sans("I")(p)||sans("I")(q) &= sans("I")(p||q) \
N * sans("I")(p) &= sans("I")(N * p)
$
since the choice of representative is arbitrary. 

In particular, we have that the following identities hold for isomorphism classes $p_i$:
$
(p_0;p_1);p_2 &= p_0;(p_1;p_2) \
(p_0||p_1)||p_2 &= p_0||(p_1||p_2) \
sans("seq")(ℕ, <_ℕ, λ n.p_n) &= p_0;sans("seq")(ℕ, <_ℕ, λ n.p_(n + 1)) \
ℕ * p &= p;(ℕ * p)
$

//TODO: sequential/parallel composition of pomset isomorphism classes
//TODO: associativity of sequential/parallel composition of pomset isomorphism classes

#definition(name: "Pomset Reduction")[
    Given a label set $L$ equipped with a distinguished element $δ ∈ L$ (which we call "*tick*"), we the pomset $(P, <_P, Φ)$ *reduces* to $(P', <_P', Φ')$, written $(P, <_P, Φ) ⇝ (P', <_P', Φ')$ if there exist $I, R$ such that
    - $(P, <_P, Φ) slash (I ∪ R) = (P', <_P', Φ')$
    - $Φ(R) ⊆ {δ}$ -- "doing nothing is OK, so long as we don't wait forever"
    - $∀t ∈ I, {p ∈ P | p <_P t} "is infinite"$ -- "if something never happens, it doesn't matter"
    - $∀t ∈ P', {p ∈ P | p <_P t} "is infinite" ==> {p ∈ P' | p <_P' t} "is infinite" ∨ t ∈ I$ -- "if something never happens, it can't happen"
    - $P$ is infinite if and only if $P'$ is infinite
    Additionally, we require that $(P', <_P', Φ')$ is nonempty (to recover reflexivity, we say $∅ ⇝ ∅$).

    We say $(P, <_P, Φ)$, $(Q, <_Q, Ψ)$ are *reduction equivalent* if they are equivalent under the relation induced by $⇝$, which is true if and only if, equivalently,
    - Both pomsets are empty, or both are nonempty and there exist $I, R$ such that:
        - $P slash (I ∪ R) =  Q slash (I ∪ R) = B$
        - $(<_P)_B = (<_Q)_B$ 
        - $Φ_B = Ψ_B$
        - $Φ(R) ∪ Ψ(R) ⊆ {δ}$
        - $∀t ∈ I ∩ P, {p ∈ P | p <_P t} "is infinite"$
        - $∀t ∈ I ∩ Q, {q ∈ Q | q <_Q t} "is infinite"$
        - $∀t ∈ P ∩ Q, {p ∈ P | p <_P t} "is infinite" <==> {q ∈ Q | q <_Q t} "is infinite"$
        - $P$ is infinite if and only if $Q$ is infinite
    - $∃r$ such that $(P, <_P, Φ) ⇝ r$ and $(Q, <_Q, Ψ) ⇝ r$
    We denote the function sending a pomset to its reduction equivalence class by $sans("R")$.

    We will often write the pomset denoting "no action," formally $({*}, -, (λ-.δ))$, as simply ${δ}$. Similarly, we will write the pomset denoting the infinite empty loop, $(ℕ, <_ℕ, δ)$, as $∞$.
]

//TODO: restate in set-form
We now prove the following properties of $sans("R")$:
- $sans("R")(sans("seq")(N, <_N, I)) ⊇ {sans("seq")(N, <_N, J) | ∀n, J_n ∈ sans("R")(I_n)}$:

    Fix $(P_i, <_i, Φ_i) ≃_⇝ (P_i', <_i', Φ_i')$. Then there exist $R_i, I_i$ such that
    $
    (P_i, <_i, Φ_i) slash (I_i ∪ R_i) = (P_i', <_i', Φ_i') slash (I_i ∪ R_i) \
    Φ_i(R_i) ∪ Φ_i'(R_i) ⊆ {δ} \
    ∀t ∈ I_i ∩ P_i, {p ∈ P | p <_i t} "is infinite" \
    ∀t ∈ I_i ∩ P_i', {p ∈ P' | p <_i t} "is infinite" \
    ∀t ∈ P_i ∩ P_i', {p ∈ P_i | p <_i' t} "is infinite" <==> {p ∈ P_i' | p <_i' t} "is infinite"
    $ 
    Taking $P = Σ_i P_i$, $P' = Σ_i P_i'$, $R = Σ_i R_i$, $I = Σ_i I_i$, we therefore have
    $
    (P, <, Φ) slash (I ∪ R) = (P', <', Φ') slash (I ∪ R) \
    Φ(R) = ⋃_i Φ_i(R_i) ⊆ {δ}
    $
    where $(P, <, Φ) = sans("seq")(N, <_N, λ n.(P_i, <_i, Φ_i))$ and $(P', <', Φ') = sans("seq")(N, <_N, λ n.(P_i', <_i', Φ_i'))$.
    Fixing arbitrary $(i, t) ∈ P$, $(j, s) ∈ P'$, we have that
    $
    (i, t) ∈ I ∩ P ==> {p ∈ P | p < (i, t)} ⊆ {(i, p) | p ∈ P_i, p <_i t} "is infinite"
    $
    $
    (j, s) ∈ I ∩ P' ==> {p ∈ P' | p <' (j, s)} ⊆ {(j, p) | p ∈ P_i', p <_j' s} "is infinite"
    $
    Now, fix $(i, t) ∈ P ∩ P'$ with $(i, t) ∉ I$. We wish to show that
    $
    {p ∈ P | p < (i, t)} "is infinite" <==> {p ∈ P' | p <' (i, t)} "is infinite" 
    $
    We note that
    $
    {p ∈ P | p < (i, t)} &= {(i, p) | p ∈ P_i, p <_i t} ∪ ⋃_(n < N)P_n \
    {p ∈ P' | p <' (i, t)} &= {(i, p) | p ∈ P_i', p <_i' t} ∪ ⋃_(n < N)P_n' \
    $
    We may hence split our proof into cases as follows:
    - Assume ${p ∈ P | p < (i, t)}$ is infinite; it hence suffices to show that ${p ∈ P' | p <' (i, t)}$ is infinite.
        - If ${(i, p) | p ∈ P_i, p <_i t}$ is infinite, then ${p ∈ P_i | p <_i t}$ must be infinite, and hence so must be ${p ∈ P_i' | p <_i' t}$ and therefore ${p ∈ P' | p <' (i, t)}$, as desired.
        - Otherwise, $⋃_(n < N)P_n$ must be infinite, so either
            - There exists $n$ with $P_n$ infinite, in which case $P_n'$ and therefore ${p ∈ P' | p <' (i, t)}$ must be infinite as well.
            - There exist infinitely many $n < N$ with $|P_n| > 0$, in which case $|P_n'| > 0$ (since no nonempty set reduces to the empty set), and therefore ${p ∈ P' | p <' (i, t)}$ must be infinite, as desired.
    - Assume now that ${p ∈ P | p < (i, t)}$ is finite; it hence suffices to show that ${p ∈ P' | p <' (i, t)}$ is finite. We note that ${(i, p) | p ∈ P_i, p <_i t}$ and $Σ_(n < N)P_n$ must be finite. The former allows us to immediately conclude that ${(i, p) | p ∈ P_i', p <_i' t}$ must be finite; hence, it remains to show that $⋃_(n < N)P_n'$ is finite. Since $⋃_(n < N)P_n$ is finite, there must only be finitely many nonempty $P_n$ with $|P_n| > 0$, and all $P_n$ must be finite. It follows that exactly these $P_n'$ are finite and nonempty, with all the other $P_n'$ empty, hence $⋃_(n < N)P_n'$ and therefore ${p ∈ P' | p <' (i, t)}$ must be finite, as desired.

    Finally, we wish to show that $P$ is infinite if and only if $P'$ is infinite. To do so, we repeat the previous logic as follows:
    - If $P$ is finite, then all but finitely many of $P_n$ must be empty, and all $P_n$ must be finite. Hence, all but finitely many $P_n'$ will be nonempty, and all $P_n'$ must be finite, so $P' = ∑P_n'$ must be finite as well.
    - If $P$ is infinite, then either
        - We have at least one infinite $P_n$, in which case $P_n'$ is also infinite and so $P'$ must be infinite
        - We have infinitely many nonempty $P_n$, which must correspond to infinitely many nonempty $P_n'$, and so again $P'$ must be infinite.

    We therefore have that $sans("seq")(N, <_N, λ n.(P_i, <_i, Φ_i)) ≃_⇝ sans("seq")(N, <_N, λ n.(P_i', <_i', Φ_i'))$ as desired.
- $sans("R")(p_0;p_1) ⊇ {q_0;q_1 | q_i ∈ sans("R")(p_i)}$: it suffices to instantiate the result for $sans("seq")$ with $N = {0, 1}$, $<_N = {0 < 1}$.
- $sans("R")(p_0||p_1) ⊇ {q_0||q_1 | q_i ∈ sans("R")(p_i)}$: it suffices to instantiate the result for $sans("seq")$ with $N = {0, 1}$, $<_N = {}$.
- $sans("R")((N, <_N) * p) ⊇ {N * q | q ∈ sans("R")(p)}$: it suffices to instantiate the result for $sans("seq")$ with $I = λ n. p$.
- For nomempty $X$, $sans("R")(X;{δ}) = sans("R")({δ};X) = sans("R")(X)$.

We now wish to show that $⋃sans("I")(sans("R")(p)) = ⋃sans("R")(sans("I")(p))$. We proceed as follows, fixing a pomset $(P, <_P, Φ)$:
- $(⊆)$: assume $(Q, <_Q, Ψ) ∈ ⋃sans("I")(sans("R")((P, <_P, Φ)))$. Then there exists a pomset $(P', <_P', Φ')$ such that
    $(P, <_P, Φ) ≃_⇝ (P', <_P', Φ')$ and $(Q', <_Q', Ψ') ≅ (P', <_P', Φ')$.

    In particular, we may find $R, I$ such that
    $
    (P, <_P, Φ) slash (I ∪ R) = (P', <_P', Φ') slash (I ∪ R) \
    Φ(R) ∪ Φ'(R) ⊆ {δ} \
    ∀t ∈ I ∩ P, {p ∈ P | p <_P t} "is infinite" \
    ∀t ∈ I ∩ P', {p ∈ P' | p <_P' t} "is infinite" \
    ∀t ∈ P ∩ P', {p ∈ P | p <_P t} "is infinite" <==> {p ∈ P' | p <_P' t} "is infinite" \
    P "is infinite" <==> P' "is infinite"
    $
    and a pomset isomorphism $φ': (P', <_P', Φ') -> (Q', <_Q', Ψ')$
    We may construct a pomset $(Q, <_Q, Ψ)$ as follows:
    $
    Q &= Q' + (P ∖ P') \
    ι_0 q <_Q ι_0 r &<=> q <_Q' r \
    ι_1 p <_Q ι_1 r &<=> p <_P r \
    ι_0 q <_Q ι_1 p &<=> φ'^(-1)(q) <_P p \
    Ψ'(ι_0 q) &= Ψ(q) = Φ'(φ'^(-1)(q)) = Φ(φ'^(-1)(q)) \
    Ψ'(ι_1 p) &= Φ(p)
    $
    where $ι_0$ is simple set-inclusion (necessary for the $⇝$ relation to work properly) while $ι_1$ introduces a tag for disambiguation.

    We may define $φ: P -> Q$ as follows:
    $
    ∀p ∈ P', φ(p) &= ι_0 φ'(p) = φ'(p) \
    ∀p ∈ P ∖ P', φ(p) &= ι_1 p
    $
    This is a bijection since $φ'$ is, and hence induces a pomset isomorphism 
    $φ: (P, <_P, Φ) -> (Q, <_Q, Ψ)$
    since
    $
    ∀p, r ∈ P',& φ(p) <_Q φ(r) <==> φ'(p) <_Q' φ'(r) <==> p <_P r \
    ∀p, r ∈ P ∖ P',& φ(p) <_Q φ(r) <==> ι_1 p <_Q' ι_1 r <==> p <_P r \
    ∀p ∈ P', r ∈ P ∖ P',&  φ(p) <_Q φ(r) <==> φ^(-1)(φ'(p)) = p <_P r \ 
    $
    Therefore, it follows that $(P, <_P, Φ) ≅ (Q, <_Q, Ψ)$, and so $(Q, <_Q, Ψ) ∈ sans("I")((P, <_P, Φ))$.
    It now remains to show that $(Q, <_Q, Ψ) ≃_⇝ (Q', <_Q', Ψ')$. 
    
    To do so, choose $R' = ∅ + R$ and $I' = ∅ + I$. We have that, since $P ∖ P' ⊆ I ∪ R$,
    $
    Q ∖ (R' ∪ I') = Q' + ((P ∖ P') ∖ I ∪ R) = Q' + ∅ = Q' \
    (<_Q)_(Q') = <_Q' = (<_Q')_(Q') \
    Ψ'(R') = Φ(R) ⊆ {δ} 
    $
    Furthermore,
    $
    ∀ι_1 t ∈ I' ∩ Q = ι_1 I, {q ∈ Q | q <_Q ι_1 t} ⊇ {q ∈ Q | φ'^(-1)(q) <_Q' t} = φ'({p ∈ P | p <_P t})
    $
    is infinite, as $t ∈ I ∩ P'$, and similarly, since $I' ∩ Q'$ is empty, it holds trivially that
    $
    ∀t ∈ I ∩ Q', {q ∈ Q' | q <_Q' t} "is infinite"
    $
    Finally, since $Q ⊇ Q'$,
    $
    ∀t ∈ Q ∩ Q' = Q', {q ∈ Q' | q <_Q' t} ⊆ {q ∈ Q | q <_Q t}
    $
    Hence, it suffices to show that, if the latter is infinite, the former is as well. To do so, assume ${q ∈ Q | q <_Q t}$ is infinite. We have that
    $
    {q ∈ Q | q <_Q t} = {q ∈ Q' | q <_Q' t} ∪ {ι_1 p | p ∈ P ∖ P', p <_P φ'^(-1)(t)}
    $
    Hence, if ${ι_1 p | p ∈ P ∖ P', p <_P φ'^(-1)(t)}$ is finite, the desired result holds trivially.
    Otherwise, we note that the following are equivalent:
    - ${ p ∈ P' | p <_P' φ^(-1)(t)} "is infinite"$
    - ${ p ∈ P | p <_P φ^(-1)(P) } "is infinite"$ (by reduction equivalence)
    - ${ q ∈ Q' | q <_Q' t } "is infinite"$ (by pomset isomorphism)
    Assume now that ${ι_1 p | p ∈ P ∖ P', p <_P φ^(-1)(t)}$ is infinite. It follows that, since this has a cardinality less than or equal to that of ${ p ∈ P | p <_P φ^(-1)(P) }$, this set must be infinite, and hence ${ q ∈ Q' | q <_Q' t } "is infinite"$, as desired.

    It remains to show that $Q$ is infinite if and only if $Q'$ is infinite. To do so, we note that
    $
    & Q "is infinite" #h(10em) \
    & <==> P "is infinite" & "by isomorphism" \
    & <==> P' "is infinite" & "by equivalence reduction" \
    & <==> Q' "is infinite" & "by isomorphism"
    $

    Hence, it follows that $(Q, <_Q, Ψ) ≃_⇝ (Q', <_Q', Ψ')$ and hence that $(Q', <_Q', Ψ') ∈ sans("R")((Q, <_Q, Ψ))$. Hence, we have that $(Q', <_Q', Ψ') ∈ ⋃sans("R")(sans("I")((P, <_P, Φ)))$, as desired.

- $(⊇)$: assume $(Q, <_Q, Ψ) ∈ ⋃sans("R")(sans("I")((P, <_P, Φ)))$. Then there exists a pomset $(Q‘, <_Q’, Ψ')$ such that
    $(P, <_P, Φ) ≅ (Q, <_Q, Ψ)$ and $(Q, <_Q, Ψ) ≃_⇝ (Q', <_Q', Ψ')$.

    In particular, we may find $R, I$ such that
    $
    (Q, <_Q, Φ) slash (I ∪ R) = (Q', <_Q', Ψ') slash (I ∪ R) \
    Φ(R) ∪ Ψ'(R) ⊆ {δ} \
    ∀t ∈ I ∩ Q, {q ∈ Q | q <_Q t} "is infinite" \
    ∀t ∈ I ∩ Q', {q ∈ Q' | q <_Q' t} "is infinite" \
    ∀t ∈ Q ∩ Q', {q ∈ Q | q <_Q t} "is infinite" <==> {q ∈ Q' | q <_Q' t} "is infinite" \
    Q "is infinite" <==> Q' "is infinite"
    $
    and a pomset isomorphism $φ: (P, <_P, Φ) -> (Q, <_Q, Ψ)$.

    We may construct a pomset $(P', <_P', Φ')$ as follows:
    $
    P' = φ^(-1)(Q') + (Q' ∖ Q) \
    ∀p, r ∈ φ^(-1)(Q'), ι_0 p <_P' ι_0 r <==> p <_P r \
    ∀q, r ∈ Q' ∖ Q, ι_1 q <_P' ι_1 r <==> q <_Q' r \
    ∀p ∈ φ^(-1)(Q'), q ∈ Q' ∖ Q, ι_0 p <_P' ι_1 q <==> φ(p) <_Q' q  \
    Φ'(ι_0 p) = Φ(p) = Ψ(φ(p)) = Ψ'(φ(p)) \
    Φ'(ι_1 q) = Ψ'(q) 
    $
    where $ι_0$ is simple set-inclusion (necessary for the $⇝$ relation to work properly) while $ι_1$ introduces a tag for disambiguation.

    We may define $φ': P' -> Q'$ as follows:
    $
    ∀p ∈ φ^(-1)(Q'), φ'(ι_0 p) &= φ(p) \
    ∀q ∈ Q' ∖ Q, φ'(ι_1 q) &= q 
    $
    This is a bijection since $φ$ is, and hence induces a pomset isomorphism 
    $φ': (P', <_P', Φ') -> (Q', <_Q', Ψ')$
    since
    $
    ∀p, r ∈ φ^(-1)(Q'), φ'(ι_0 p) <_Q' φ'(ι_0 r) <==> φ(p) <_Q' φ(r) <==> p <_P r <==> ι_0 p <_P' ι_0 r \
    ∀q, r ∈ Q' ∖ Q, φ'(ι_1 q) <_Q' φ'(ι_1 r) <==> p <_Q' r <==> ι_1 q <_P' ι_1 r \
    ∀p ∈ φ^(-1)(Q'), q ∈ Q' ∖ Q, φ'(ι_0 p) <_Q' φ'(ι_1 q) <==> φ(p) <_Q' q <==> ι_0 p <_P' ι_1(q)
    $
    Therefore, it follows that $(P', <_P', Φ') ≅ (Q', <_Q', Ψ')$, and so $(Q', <_Q', Ψ') ∈ sans("I")((P', <_P', Φ'))$.
    It now remains to show that $(P, <_P, Φ) ≃_⇝ (P', <_P', Φ')$. 

    Choose $R' = φ^(-1)(R) + R$, $I' = φ^(-1)(I) + I$. We have that
    $
    P' ∖ (R' ∪ I') &= (φ^(-1)(Q') ∖ φ^(-1)(R ∪ I)) + ((Q' ∖ Q) ∖ (R ∪ I))
    \ &= φ^(-1)(Q) ∖ φ^(-1)(R ∪ I) + ∅
    \ &= P ∖ φ^(-1)(R ∪ I)
    \ &= P ∖ (R' ∪ I') \
    Φ'(R') ∪ Φ(R') &= Φ(φ^(-1)(R)) ∪ Ψ'(R) ∪ Φ(φ^(-1)(R)) ⊆ {δ} ∪ {δ} ∪ {δ} = {δ} \
    $
    Furthermore,
    $
    ∀ t ∈ I' ∩ P ⊆ φ^(-1)(I), 
        {p ∈ P | p <_P t} =
        φ^(-1)({q ∈ Q | q <_Q φ(t)})
        "is infinite"
    $
    since $φ(t) ∈ I$.
    Similarly, fixing $t ∈ I' ∩ P' ⊆ φ^(-1)(I) + I$,
    - If $t = ι_0 r$, then
        $
        {p ∈ P' | p <_P' ι_0 r} ⊆ {ι_0 φ^(-1)(q) | q <_Q' φ(r)}
        $
        which is infinite since $φ$ is a bijection and $r ∈ φ^(-1)(I) ==> φ(r) ∈ I$
    - If $t = ι_1 r$, then
        $
        {p ∈ P' | p <_P' ι_1 r} ⊆ {ι_0 φ^(-1)(q) | q ≤_Q' r}
        $
        which is again infinite since $φ$ is a bijection and $r ∈ I$

    Given $t ∈ P ∩ P' = φ^(-1)(Q')$, we have that 
    $
    {p ∈ P' | p <_P' t} &= {p ∈ φ^(-1)(Q') | p <_P t} ∪ ι_1{q ∈ Q' ∖ Q | q <_Q' φ(t)} \
        &= φ'({q ∈ Q' | q <_Q' φ(t)})
    $
    - If ${p ∈ P | p <_P t}$ is infinite, then by isomorphism ${q ∈ Q | q <_Q φ(t)}$ must be infinite, and by definition $φ(t) ∈ Q'$; hence, by reduction-equivalence, ${q ∈ Q' | q <_Q' φ(t)}$ and therefore ${p ∈ P' | p <_P' t}$ must be infinite.
    - Likewise, if ${p ∈ P | p <_P t}$ is finite, then by isomorphism ${q ∈ Q | q <_Q φ(t)}$ must be finite, and by definition $φ(t) ∈ Q'$; hence, by reduction-equivalence, ${q ∈ Q' | q <_Q' φ(t)}$ and therefore ${p ∈ P' | p <_P' t}$ must be finite.

    Finally, it remains to show that $P$ is infinite if and only if $P'$ is infinite. To do so, we note that
    $
    & P "is infinite" #h(10em) \
    & <==> Q "is infinite" & "by isomorphism" \
    & <==> Q' "is infinite" & "by equivalence reduction" \
    & <==> P' "is infinite" & "by isomorphism"
    $

    It follows that $(P', <_P', Φ') ≃_⇝ (P, <_P, Φ)$ and therefore that $(P', <_P', Φ') ∈ sans("R")((P, <_P, Φ))$. Therefore, we must have that $(Q, <_Q, Ψ) ∈ ⋃sans("I")(sans("R")((P, <_P, Φ)))$, as desired.

#definition(name: "Pomset Equivalence")[
    We say pomsets $(P, <_P, Φ)$, $(Q, <_Q, Ψ)$ are *equivalent* if $(P, <_P, Φ) ≃ (Q, <_Q, Ψ)$ where $≃$ is the smallest equivalence relation contaning $⇝$ and $≅$. We denote the function sending a pomset to its equivalence class by $sans("P")$.
]

We note that we have
$
sans("P")(p) = ⋃sans("I")(sans("R")(p)) = ⋃sans("R")(sans("I")(p))
$
We may now prove the following properties of $sans("P")$:
- $sans("P")(sans("seq")(N, <_N, I)) ⊇ {sans("seq")(N, <_N, J) | ∀n, J_n ∈ sans("P")(I_n)}$: 
    We have
    $
    sans("P")(sans("seq")(N, <_N, I)) &= ⋃sans("R")(sans("I")(sans("seq")(N, <_N, I))) 
        \ & ⊇ ⋃sans("R")({sans("seq")(N, <_N, J) | ∀n, J_n ∈ sans("I")(I_n)})
        \ & = ⋃{sans("R")(sans("seq")(N, <_N, J))) | ∀n, J_n ∈ sans("I")(I_n)}
        \ & ⊇ ⋃{{sans("seq")(N, <_N, K) | ∀n, K_n ∈ sans("R")(J_n)} | ∀n, J_n ∈ sans("I")(I_n)}
        \ & = {sans("seq")(N, <_N, K) | ∀n, K_n ∈ sans("R")(J_n), J_n ∈ sans("I")(I_n)}
        \ & = {sans("seq")(N, <_N, K) | ∀n, K_n ∈ ⋃sans("R")(sans("I")(I_n))}
        \ & = {sans("seq")(N, <_N, K) | ∀n, K_n ∈ sans("P"(I_n))}
    $
- $sans("P")(p_0;p_1) ⊇ {q_0;q_1 | q_i ∈ sans("P")(p_i)}$: it suffices to instantiate the result for $sans("seq")$ with $N = {0, 1}$, $<_N = {0 < 1}$.
- $sans("P")(p_0||p_1) ⊇ {q_0||q_1 | q_i ∈ sans("P")(p_i)}$: it suffices to instantiate the result for $sans("seq")$ with $N = {0, 1}$, $<_N = {}$.
- $sans("P")((N, <_N) * p) ⊇ {N * q | q ∈ sans("P")(p)}$: it suffices to instantiate the result for $sans("seq")$ with $I = λ n. p$.

//TODO: Greek syntax, operator definitions

//TODO: clean this
We define $sans("Pom")(L)$ to be the set of equivalence classes of _nonempty_ pomsets with the finite height property over the label set $L$, and $sans("Pom")_0(L)$ to be $sans("Pom")(L)$ plus the equivalence class of the empty pomset. Letting $cal(A)_r$ denote the set of global reads, $cal(A)_w$ the set of global writes, and $cal(A) = cal(A)_r ∪ cal(A)_w ∪ {δ}$, we write $sans("Pom")(cal(A))$ as $sans("Pom")$, the set of *program orders*, and $sans("Pom")_0(cal(A))$ as $sans("Pom")_0$.

= The Program Order Monad

We begin by defining the following monad:
$
sans("PO")(A) &= cal(P)(sans("Pom") × A^⊥) \
sans("ret")_(sans("PO")) a &= {({δ}, a)} \
sans("bind")_(sans("PO"))(A, f) &= {(p;q, b) | (p, a) ∈ A ∧ a ≠ ⊥ ∧ (q, b) ∈ f(a)}
$
where $A^⊥ = A ∪ {⊥}$ and, for $f: A -> sans("PO")(B)$, we define $f^⊥(⊥) = {({δ}, ⊥)}$ (with $∀a ∈A, f^⊥(a) = f(a)$) .
This is a monad since:
$
sans("bind")(sans("ret") a, f) 
    &= {(({δ};q), b) | (q, b) ∈ f^⊥(a)} = {(q, b) | (q, b) ∈ f^⊥(a)} = f^⊥(a) \
sans("bind")(A, sans("ret")) 
    &= {((p;{δ}), a) | (p, a) ∈ A } = {(p, a) | (p, a) ∈ A} = A \
sans("bind")(sans("bind")(A, f), g)
    &= {(t;r, c) | (t, b) ∈ {(p;q, b) | (p, a) ∈ A ∧ (q, b) ∈ f^⊥(a)} ∧ (r, c) ∈ g^⊥(b)} \
    &= {((p;q);r, c) | (p, a) ∈ A ∧ (q, b) ∈ f^⊥(a) ∧ (r, c) ∈ g^⊥(b)} \
    &= {(p;(q;r), c) | (p, a) ∈ A ∧ (q, b) ∈ f^⊥(a) ∧ (r, c) ∈ g^⊥(b))} \
    &= {(p;s, c) | (p, a) ∈ A ∧ (s, c) ∈ { (q;r, c) | (q, b) ∈ f^⊥(a) ∧ (r, c) ∈ g^⊥(b)}} \
    &= sans("bind")(A, λ x.sans("bind")(f(x), g))
$
//TODO: what does ({δ}, ⊥) mean? What about (∞, a)?
This monad is trivially strong since it is an endofunctor on $sans("Set")$. Hence, its Kliesli category $sans("Set")_(sans("PO"))$ is a premonoidal category inheriting coproducts from $sans("Set")$. Therefore, to obtain an `isotope` model, it suffices to equip $sans("Set")_(sans("PO"))$ with an iterative structure, as follows: let $f: X ->_(sans("PO")) A + X$ be a morphism in the Kleisli category (i.e., take $f: X -> sans("PO")(A + X)$). We define
$
f^†: X ->_(sans("PO")) A = λ x. I^ω(f, x) ∪ ⋃_n I^n(f, x)
$ 
where
$
I^0(f, x) &= {({δ}, x)} \
I^(n + 1)(f, x) &= {(p;q, s) | (p, t) ∈ I^n(x) ∧ (q, ι_1 s) ∈ f(t)} \
I^ω(f, x) &= {(sans("seq")(ℕ, <_ℕ, λ n. B_(n 0)), ⊥) | B_0 = ({δ}, x), ∀n, B_(n + 1) ∈ f(B_(n 1))}
$

TODO: $f^†$ is an Elgot algebra

= The TSO Monad

Let $sans("Ls") = [cal(A)_w]$ denote the type of list of _write_ actions; given a list $ℓ ∈ sans("Ls")$, we define
$
sans("P")(ℓ) = sans("P")({({0..sans("len")(ℓ)}, <_ℕ, λ n. ℓ[n])}): sans("Pom")_0(cal(A)_w)
$
to map a list $ℓ$ to the appropriate (potentially empty) linear pomset.
We may now define the $sans("TSO")$ monad by applying the state transformer over $sans("Ls")$ to the $sans("PO")$ monad as follows::
$
sans("TSO")(A) &= sans("Ls") -> sans("PO")(A × sans("Ls")) =
    sans("Ls") -> cal(P)(sans("Pom") × A × sans("Ls"))
$

//TODO: pure
//TODO: bind
//TODO: "this is a monad because state transformer"

//TODO: Set so therefore
//TODO: coproduct structure, and also
//TODO: monadic strength, and hence
//TODO: premonoidal structure

//TODO: Elgot algebra (lifting?)

//TODO: consider !FUN! theorems like DRF-SC
//TODO: think about semantic v syntactic dependencies
//TODO: go read PwP

#bibliography("references.bib")